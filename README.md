# README #

'basicStack.m' contains the necessary class set up for outcooupling power at a particular kx value.

Other '.m' files starting with 'Example' are files guiding the user through different structure set-ups.

### What is this repository for? ###

* This repo calculates the output power from a layered slab with an active emitting layer inside. Its current version 2.0.1 supports the following utilities:
 
 Angular distribution power distribution
 
 Total outcoupled energy either into bottom or top directions.

### How do I get set up? ###

This is a Matlab based repo - you don't need to set it up, just copy-paste-run!

### Contribution guidelines ###

Pleaes contact Yufei Shen if you would like to see certain utilities incorporated into this package.

### Who do I talk to? ###

* yufei@psu.edu
