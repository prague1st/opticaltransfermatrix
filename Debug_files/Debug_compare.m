% Device structure (unit: nm) :
%   sub0   |   0     |     1      |    2    |      3       |      4       |    5    |     6   |   7      |   8
%    Air   |  Glass  |  ITO (100) | HTL (x) | EML:HTL (15) | EML:ETL (15) | ETL (y) | Liq (2) | Al (80)  |  Air
%
% The Glass-air interface is treated incoherently.
% Always assume EML:HTL and EML:ETL have the same thickness

%===========================================================================
%	 Yufei						 02/27/2017			   Version 1.0
%===========================================================================
close all;
%clear all;
tic;

FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';
cd(FPATH)

%% ============================= read in constants =============================
Emission_spectrum = dlmread('Irppy_emission.txt');
index_0 = dlmread('./misc_reference/BK7.txt');
index_1 = dlmread('./misc_reference/ITO.txt');
index_2 = dlmread('./HTLs/TAPC and TefAF1600 30pct.txt','',2,0);
index_3 = dlmread('./EMLs/TAPC, Irppy 8pct, and TefAF1600 30pct.txt','',2,0);
index_4 = dlmread('./EMLs/BPhen, Irppy 8pct, and TefAF1600 30pct.txt','',2,0);
index_5 = dlmread('./ETLs/BPhen and TefAF1600 30pct.txt','',2,0);
index_6 = dlmread('./EIL/Liq.txt','',2,0);
index_7 = dlmread('./metal/Aluminum.txt','',1,0);
% ============================= End read in constants =========================

%% ========================= General system settings =======================
d1 = 100;
d2 = 30;
d3 = 15;
d4 = 15;
d5 = 30;
d6 = 2;
d7 = 80;        % Layer thicknesses

num_dipole = 1;         % Total number of dipole layers
% =========================== End general system settings ===================

%% set wave vectors in each layer
wl = 300;

nsub0 = 1;         % air beneath glass substrate
n0 = spline(index_0(:,1),index_0(:,2),wl);
n1 = spline(index_1(:,1),index_1(:,2),wl);
n2 = spline(index_2(:,1),index_2(:,2),wl) + 1i*spline(index_2(:,1),index_2(:,3),wl);
n3 = spline(index_3(:,1),index_3(:,2),wl) + 1i*spline(index_3(:,1),index_3(:,3),wl);
n4 = spline(index_4(:,1),index_4(:,2),wl) + 1i*spline(index_4(:,1),index_4(:,3),wl);
n5 = spline(index_5(:,1),index_5(:,2),wl) + 1i*spline(index_5(:,1),index_5(:,3),wl);
n6 = spline(index_6(:,1),index_6(:,2),wl) + 1i*spline(index_6(:,1),index_6(:,3),wl);
n7 = spline(index_7(:,1),index_7(:,2),wl) + 1i*spline(index_7(:,1),index_7(:,3),wl);
n8 = 1;            % air

k_air = 2*pi/wl;
k0 = n0*k_air;
k1 = n1*k_air;
k2 = n2*k_air;
k3 = n3*k_air;
k4 = n4*k_air;
k5 = n5*k_air;
k6 = n6*k_air;
k7 = n7*k_air;
k8 = n8*k_air;
ksub0 = nsub0*k_air;

position = 7.5;

angled = 85;        % angle in degrees
angle = angled*pi/180;

% Total_power_xy = zeros(length(x),length(y));

% parpool(4)
%% Begin x and y sweep:
IntenTotalTE = zeros(length(angle),1);
IntenTotalTM = zeros(length(angle),1);
%% Beging theta loop
for count_angle = 1:length(angle)
    theta = angle(count_angle);
    q0 = sqrt(k0^2-(k_air*sin(theta))^2);
    q1 = sqrt(k1^2-(k_air*sin(theta))^2);
    q2 = sqrt(k2^2-(k_air*sin(theta))^2);
    q3 = sqrt(k3^2-(k_air*sin(theta))^2);
    q4 = sqrt(k4^2-(k_air*sin(theta))^2);
    q5 = sqrt(k5^2-(k_air*sin(theta))^2);
    q6 = sqrt(k6^2-(k_air*sin(theta))^2);
    q7 = sqrt(k7^2-(k_air*sin(theta))^2);
    q8 = sqrt(k8^2-(k_air*sin(theta))^2);
    qsub0 = sqrt(ksub0^2-(k_air*sin(theta))^2);
%% Begin dipole position loop
    for count_posi = 1:length(position)
        depth = position(count_posi);           % dipole depth in EML:HTL and EML:ETL

        % ============================ Begin TE polarization case: ===============================
        A_TE = sqrt(3/16/pi);
        L_3u = [exp(1i*q3*depth),0;0,exp(-1i*q3*depth)];
        I_34_TE = [1,(q3-q4)/(q3+q4);(q3-q4)/(q3+q4),1]/(2*q3/(q3+q4));
        L_4u = [exp(1i*q4*d4),0;0,exp(-1i*q4*d4)];
        I_45_TE = [1,(q4-q5)/(q4+q5);(q4-q5)/(q4+q5),1]/(2*q4/(q4+q5));
        L_5u = [exp(1i*q5*d5),0;0,exp(-1i*q5*d5)];
        I_56_TE = [1,(q5-q6)/(q5+q6);(q5-q6)/(q5+q6),1]/(2*q5/(q5+q6));
        L_6u = [exp(1i*q6*d6),0;0,exp(-1i*q6*d6)];
        I_67_TE = [1,(q6-q7)/(q6+q7);(q6-q7)/(q6+q7),1]/(2*q6/(q6+q7));
        L_7u = [exp(1i*q7*d7),0;0,exp(-1i*q7*d7)];
        I_78_TE = [1,(q7-q8)/(q7+q8);(q7-q8)/(q7+q8),1]/(2*q7/(q7+q8));
        Aa_TE = L_3u * I_34_TE * L_4u * I_45_TE * L_5u * I_56_TE * L_6u * I_67_TE * L_7u * I_78_TE;

        L_3d = [exp(-1i*q3*(d3-depth)),0;0,exp(1i*q3*(d3-depth))];
        I_32_TE = [1,(q3-q2)/(q3+q2);(q3-q2)/(q3+q2),1]/(2*q3/(q3+q2));
        L_2d = [exp(-1i*q2*d2),0;0,exp(1i*q2*d2)];
        I_21_TE = [1,(q2-q1)/(q2+q1);(q2-q1)/(q2+q1),1]/(2*q2/(q2+q1));
        L_1d = [exp(-1i*q1*d1),0;0,exp(1i*q1*d1)];
        I_10_TE = [1,(q1-q0)/(q1+q0);(q1-q0)/(q1+q0),1]/(2*q1/(q1+q0));
        Bb_TE = L_3d * I_32_TE * L_2d * I_21_TE * L_1d * I_10_TE;

        E0_TE_h = -(Aa_TE(2,2)+Aa_TE(1,2))*A_TE/(Aa_TE(2,2)*Bb_TE(1,1)-Aa_TE(1,2)*Bb_TE(2,1));
        IntenTotalTE = 2/3*real(n0/n3*abs(E0_TE_h)^2*q0^2/q3^2);

        % ============================ Begin TM polarization case: ===============================
        A_TM_v = sqrt(3/8/pi)*k_air*sin(theta)/k3;
        A_TM_h = sqrt(3/16/pi)*q3/k3;

        I_34_TM = [1,(n4^2*q3-n3^2*q4)/(n4^2*q3+n3^2*q4);(n4^2*q3-n3^2*q4)/(n4^2*q3+n3^2*q4),1]/((2*n3*n4*q3)/(n4^2*q3+n3^2*q4));
        I_45_TM = [1,(n5^2*q4-n4^2*q5)/(n5^2*q4+n4^2*q5);(n5^2*q4-n4^2*q5)/(n5^2*q4+n4^2*q5),1]/((2*n4*n5*q4)/(n5^2*q4+n4^2*q5));
        I_56_TM = [1,(n6^2*q5-n5^2*q6)/(n6^2*q5+n5^2*q6);(n6^2*q5-n5^2*q6)/(n6^2*q5+n5^2*q6),1]/((2*n5*n6*q5)/(n6^2*q5+n5^2*q6));
        I_67_TM = [1,(n7^2*q6-n6^2*q7)/(n7^2*q6+n6^2*q7);(n7^2*q6-n6^2*q7)/(n7^2*q6+n6^2*q7),1]/((2*n6*n7*q6)/(n7^2*q6+n6^2*q7));
        I_78_TM = [1,(n8^2*q7-n7^2*q8)/(n8^2*q7+n7^2*q8);(n8^2*q7-n7^2*q8)/(n8^2*q7+n7^2*q8),1]/((2*n7*n8*q7)/(n8^2*q7+n7^2*q8));
        Aa_TM = L_3u * I_34_TM * L_4u * I_45_TM * L_5u * I_56_TM * L_6u * I_67_TM * L_7u * I_78_TM;

        I_32_TM = [1,(n2^2*q3-n3^2*q2)/(n2^2*q3+n3^2*q2);(n2^2*q3-n3^2*q2)/(n2^2*q3+n3^2*q2),1]/((2*n3*n2*q3)/(n2^2*q3+n3^2*q2));
        I_21_TM = [1,(n1^2*q2-n2^2*q1)/(n1^2*q2+n2^2*q1);(n1^2*q2-n2^2*q1)/(n1^2*q2+n2^2*q1),1]/((2*n2*n1*q2)/(n1^2*q2+n2^2*q1));
        I_10_TM = [1,(n0^2*q1-n1^2*q0)/(n0^2*q1+n1^2*q0);(n0^2*q1-n1^2*q0)/(n0^2*q1+n1^2*q0),1]/((2*n1*n0*q1)/(n0^2*q1+n1^2*q0));
        Bb_TM = L_3d * I_32_TM * L_2d * I_21_TM * L_1d * I_10_TM;

        E0_TM_v = (Aa_TM(2,2)-Aa_TM(1,2))*A_TM_v/(Aa_TM(2,2)*Bb_TM(1,1)-Aa_TM(1,2)*Bb_TM(2,1));
        E0_TM_h = -(Aa_TM(2,2)+Aa_TM(1,2))*A_TM_h/(Aa_TM(2,2)*Bb_TM(1,1)-Aa_TM(1,2)*Bb_TM(2,1));

        IntenTotalTM = 1/3*real(n0/n3*abs(E0_TM_v)^2*q0^2/q3^2) + 2/3*real(n0/n3*abs(E0_TM_h)^2*q0^2/q3^2);
    end
end
IntenTotal = IntenTotalTE + IntenTotalTM;
disp(IntenTotal)

                            
toc;