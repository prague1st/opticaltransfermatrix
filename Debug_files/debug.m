% Outcoupling efficiency with embedded dipoles
% Vary thickness of HTL and ETL
%===========================================================================
%	 yufei.shen						 02/27/2017			   Version 1.0
%	 yufei.shen						 10/20/2017			   Version 2.0 - use basicStack class
%===========================================================================

close all;
clear all;
tic;

% FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';           % Please set this to your local path
% cd(FPATH)

wl              = 500;                                                  % Emission wavelength
dipoleLayer     = 4;                                                    % which layer dipole is in
materials       = {'BK7','ITO','TAPC', 'Alq3','Liq','Al'};       % Material stack structure, from bottom to top


% Total_power     = zeros(1,length(range));
% kx_resolved_totalPower  = zeros(1, length(range));
E0_TE_h     = [];
E0_TM_h     = [];
E0_TM_v     = [];


Ds              = [ nan,  100,   25, 200,    2,   100 ];
dipoleDepth     = Ds(4)/2;
OLED            = basicStack( wl, dipoleLayer, dipoleDepth, materials, Ds, 0);
OLED.setIndices();
OLED.setKs();

division        = 100000;
kx_lower_ratio  = 0.95;
if OLED.Ks(OLED.dipole_layer)*kx_lower_ratio > OLED.Ks(1)
    k_para          = linspace(0, OLED.Ks(1), 500);
else
    k_para=[];
     k_para          = linspace(0, OLED.Ks(OLED.dipole_layer)*kx_lower_ratio, 500);
%     k_para          = [k_para, linspace(OLED.Ks(OLED.dipole_layer)*kx_lower_ratio, OLED.Ks(OLED.dipole_layer)/kx_lower_ratio, division)];
     k_para          = [k_para, linspace(OLED.Ks(OLED.dipole_layer)/kx_lower_ratio, OLED.Ks(1), 500)];
end
    
for kx_iter = 1:length(k_para)
    Kx              = k_para(kx_iter);
    OLED.Kx         = Kx;
    OLED.setQs();
    OLED.setSourceTerm();
    OLED.constructInterfaceMatrices();
    OLED.constructPropMatrix();
    OLED.constructSourceMatrix();
    OLED.calcE();
    OLED.EnergyOC();
%         E0_TE_h     = [E0_TE_h, OLED.E0_TE_h];
%         E0_TM_h     = [E0_TM_h, OLED.E0_TM_v];
%         E0_TM_v     = [E0_TM_v, OLED.E0_TM_h];
    kx_resolved_totalPower(kx_iter)     = OLED.Power_hor_sub+OLED.Power_ver_sub;
end
%     plot(k_para, E0_TE_h)
%     hold on
%     plot(k_para, E0_TM_h)
%     hold on
%     plot(k_para, E0_TM_v)
%     hold off
%     legend('TE_h','TM_h','TM_v')
Total_power        = trapz( asin(k_para/OLED.Ks(1)), 2*pi*k_para/OLED.Ks(1).*kx_resolved_totalPower )
plot(k_para,2*pi*k_para/OLED.Ks(1).*kx_resolved_totalPower)



toc;