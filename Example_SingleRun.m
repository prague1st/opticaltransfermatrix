% Outcoupling efficiency with embedded dipoles
% This file wraps stackBasic class and outputs the desired results.
%===========================================================================
%	 yufei.shen						 02/27/2017			   Version 1.0
%	 yufei.shen						 10/20/2017			   Version 2.0 - use basicStack class
%===========================================================================

close all;
clear all;
tic;

% FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';
% cd(FPATH)

wl              = 300;                          % Emission wavelength
dipoleLayer     = 4;                            % Specify dipole is in layer 4 (Irppy_TAPC_Teflon in this example)
materials       = {'BK7','ITO','TAPC_Teflon', 'Irppy_TAPC_Teflon','Irppy_BPhen_Teflon','BPhen_Teflon','Liq','Al'};
Ds              = [ nan,  100,      30,                15,               15,                   30,      2,   80 ];
Kpara           = 2*pi/wl*sind(85);             % Specify the parallel wavevector Kx
dipoleDepth     = Ds(4)/2;                      % Depth of dipole layer inside layer 4, from top of that layer.

OLED            = basicStack( wl, dipoleLayer, dipoleDepth, materials, Ds, Kpara);          % Construct a 'basicStack' class object

% ============= Lines 23-31: run class methods one by one to calculate outcoupling power. ==================
% For the function of each class method, please refer to 'basicStack.m' file
OLED.setIndices();                                          
OLED.setKs();
OLED.setQs();
OLED.setSourceTerm();
OLED.constructInterfaceMatrices();
OLED.constructPropMatrix();
OLED.constructSourceMatrix();
OLED.calcE();
OLED.EnergyOC();
% ==========================================================================================================

disp(OLED.Power_hor_sub+OLED.Power_ver_sub)

toc;