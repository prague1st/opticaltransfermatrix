% Outcoupling efficiency with embedded dipoles
% Vary thickness of HTL and ETL
%===========================================================================
%	 yufei.shen						 02/27/2017			   Version 1.0
%	 yufei.shen						 11/10/2017			   Version 2.0 - use basicStack class
%===========================================================================

close all;
clear all;
tic;

% FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';           % Please set this to your local path
% cd(FPATH)

wl              = 500;                                                  % Emission wavelength
dipoleLayer     = 4;                                                    % Specify which layer dipole is in. Here it is in 'Alq3_Teflon'
materials       = {'BK7','ITO','TAPC', 'Alq3','Liq','Al'};              % Material stack structure, from bottom to top

d_HTL   = 20;       % HTL thickness
d_ETL   = 100;      % ETL thickness
Total_power     = zeros(length(d_HTL),length(d_ETL));

% Construct the object outside for loop
OLED            = basicStack( wl, dipoleLayer, 1, materials, [nan, ones(1,length(materials)-1)], nan);
OLED.setIndices();
OLED.setKs();
k_para  = linspace(0, OLED.Ks(1), 1000);
kx_resolved_totalPower  = zeros(1, length(k_para));

for kx_iter = 1:length(k_para)
    Kx              = k_para(kx_iter);
    OLED.Kx         = Kx;
    % Set thickness of each layer. Same order as in 'materials':
    Ds              = [ nan,  100,   d_HTL,  d_ETL,    2,   100 ];  
    OLED.d          = [Ds, nan];
    dipoleDepth     = Ds(4)/2;                 % Dipole placed in the middle of EML
    OLED.dipole_depth   = dipoleDepth;
    OLED.setQs();
    OLED.setSourceTerm();
    OLED.constructInterfaceMatrices();
    OLED.constructPropMatrix();
    OLED.constructSourceMatrix();
    OLED.calcE();
    OLED.EnergyOC();
    kx_resolved_totalPower(kx_iter)     = OLED.Power_hor_sub+OLED.Power_ver_sub;
end
angle_degree    = asind(k_para/OLED.Ks(1));

plot(angle_degree, kx_resolved_totalPower)
set(gca, 'fontsize',24)
xlabel('Angle in substrate','fontsize',30)
ylabel('Power', 'fontsize',30)

filename = 'test.mat';
save(filename)

toc;