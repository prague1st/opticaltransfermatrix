% Outcoupling efficiency with embedded dipoles
% Vary thickness of HTL and ETL
%===========================================================================
%	 yufei.shen						 02/27/2017			   Version 1.0
%	 yufei.shen						 11/10/2017			   Version 2.0 - use basicStack class
%===========================================================================

close all;
clear all;
tic;

% FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';           % Please set this to your local path
% cd(FPATH)

wl              = 500;                                                  % Emission wavelength
dipoleLayer     = 4;                                                    % Specify which layer dipole is in. Here it is in 'Alq3'
materials       = {'BK7','ITO','TAPC', 'Alq3','Liq','Al'};              % Material stack structure, from bottom to top

num_HTL = 31;
num_ETL = 30;
d_HTL   = linspace(5,400,num_HTL);
d_ETL   = linspace(5,400,num_ETL);

Total_power     = zeros(length(d_HTL),length(d_ETL));

% Construct the object outside for loop
OLED            = basicStack( wl, dipoleLayer, 1, materials, [nan, ones(1,length(materials)-1)], nan);
OLED.setIndices();
OLED.setKs();
k_para  = linspace(0, OLED.Ks(1), 200);
for HTL_iter = 1:length(d_HTL)
    fprintf( 'Calculation percentage: %.2f%%\n', num_ETL*(HTL_iter-1)/(num_HTL*num_ETL)*100 );
    for ETL_iter = 1:length(d_ETL)
        kx_resolved_totalPower  = zeros(1, length(k_para));
        for kx_iter = 1:length(k_para)
            Kx              = k_para(kx_iter);
            OLED.Kx         = Kx;
            % Set thickness of each layer. Same order as in 'materials':
            Ds              = [ nan,  100,   d_HTL(HTL_iter),  d_ETL(ETL_iter),    2,   100 ];  
            OLED.d          = [Ds, nan];
            dipoleDepth     = Ds(4)/2;
            OLED.dipole_depth   = dipoleDepth;
            OLED.setQs();
            OLED.setSourceTerm();
            OLED.constructInterfaceMatrices();
            OLED.constructPropMatrix();
            OLED.constructSourceMatrix();
            OLED.calcE();
            OLED.EnergyOC();
            OLED.Energy_bottom_air();
            kx_resolved_totalPower(kx_iter)     = OLED.Power_hor_sub_escape+OLED.Power_ver_sub_escape;
        end
        outcoupleMask   = k_para < OLED.k_air;
        Total_power(HTL_iter, ETL_iter)         = trapz( asin(k_para(outcoupleMask)/OLED.k_air), ...
                                                  2*pi*k_para(outcoupleMask)/OLED.k_air.*kx_resolved_totalPower(outcoupleMask) );   % Eqn 19 of Benisty paper
        % Total_power(HTL_iter, ETL_iter)         = trapz( asin(k_para/OLED.Ks(1)), 2*pi*k_para/OLED.Ks(1).*kx_resolved_totalPower );
    end
end

[x_grid, y_grid] = meshgrid(d_HTL,d_ETL);
surf(x_grid,y_grid,Total_power')
xlabel('HTL thickness (nm)','fontsize',30)
ylabel('ETL thickness (nm)','fontsize',30)
set(gca,'fontsize',30)
az = 0;
el = 90;
view(az, el);
axis tight
grid off
shading interp
colorbar

filename = 'test.mat';
save(filename)


toc;