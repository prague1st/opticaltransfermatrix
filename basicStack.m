
% This script defines a class for the layer-stacked structure.

%===========================================================================
%	 yufei.shen						 10/20/2017			   Version 1.0
%    yufei.shen                      10/23/2017            Version 1.0.1: make class properties public for faster initialization 
%    yufei.shen                      11/10/2017            Version 1.0.2: Add utilities: angluar dependence and bottom outcoupling to air through glass
%    yufei.shen                      02/10/2018            Version 1.0.3: Update 'Energy_bottom_air' function, and add material lookup path
%===========================================================================

classdef basicStack < handle
    properties( SetAccess = public )
        lambda                      % Emission, or incident light wavelength
        matList                     % a ordered cell array of strings
        n                           % an ordered array of refractive index values
        d                           % an ordered array of layer thicknesses
        k_air                       % wavevector in air
        Ks                          % an ordered array of total wavevectors inside each layer
        Kx                          % a (float point) value of incoming parallel wavevectors
        Qs                          % an ordered array storing q-vectors
        dipole_layer                % an int says which layer the dipole is in (refer to structure figure)
        dipole_depth                % a float indicates the location of the dipole, from the top of the layer it is in
        A_TE                        % source term for TE, Eqns. (6) and (7) in Benisty paper
        A_TM_v                      % source term for TM, vertical dipole moment alignment
        A_TM_h                      % source term for TM, horizontal dipole moment alignment
        I_TE_up                     % Starting from this line: cell arrays storing interface and prop matrices
        I_TE_dn
        I_TM_up
        I_TM_dn
        L_up
        L_dn
        Aa_TE                       % Starting from this line: A and B matrices as defined by Benisty in Eqn. (15), ...
        Aa_TM                       % which is equivalent to the S matrix in Eqn. (8) in Inganas paper.
        Bb_TE
        Bb_TM
        E0_TE_h                     % Final E-field for: 1. TE, horizontal dipole; 2. TM, vertical dipole, 3. TM, horizontal dipole
        E0_TM_v                     % *************** E0 here is for emission into substrate ***************
        E0_TM_h
        E2_TE_h                     % Final E-field for: 1. TE, horizontal dipole; 2. TM, vertical dipole, 3. TM, horizontal dipole
        E2_TM_v                     % *************** E2 here is for emission up to air ***************
        E2_TM_h
        Power_hor_sub_TE            % Total bottom outcoupled power into substrate for horizontal dipole -- TE polarization
        Power_hor_sub_TM            % Total bottom outcoupled power into substrate for horizontal dipole -- TM polarization
        Power_hor_sub               % Total bottom outcoupled power into substrate for horizontal dipole
        Power_ver_sub               % Total bottom outcoupled power into substrate for vertical dipole
        Power_hor_top_TE            % Total top outcoupled power into air for horizontal dipole -- TE polarization
        Power_hor_top_TM            % Total top outcoupled power into air for horizontal dipole -- TM polarizaton
        Power_hor_top               % Total top outcoupled power into air for horizontal dipole
        Power_ver_top               % Total top outcoupled power into air for vertical dipole
        Power_hor_sub_escape        % Downward outcoupled power into air (vertical dipole), substrate-air interface is considered Incoherently
        Power_ver_sub_escape        % Downward outcoupled power into air (horizontal dipole), substrate-air interface is considered Incoherently
    end
    
    methods
        function self = basicStack( wavelength, dipoleLayer, dipoleDepth, materials, Ds, Kpara )
            % Constructor for the basicStack class
            self.lambda             = wavelength;
            self.matList            = materials;
            if isnan(Ds(1))
                self.d              = [Ds, nan];           % Set thickness of last layer (air) to nan
            else
                self.d              = [nan, Ds, nan];      % Set thicknesses of both first layer (substrate) and last layer (air) to nan
            end
            self.Kx                 = Kpara;
            self.k_air              = 2*pi/self.lambda;
            if dipoleLayer
                self.dipole_layer   = dipoleLayer;
                self.dipole_depth   = dipoleDepth;
            end
        end
        function setIndices(self)
            % Set refractive index for each layer at lambda using a cubic spline approach
            self.n       = [];
            for iter = 1:length(self.d)-1
                matName                 = self.matList{iter};
                dispersion              = basicStack.getDispersion( matName );
                if size(dispersion,2)==3            % material refractive index has both n and k
                    index   = spline(dispersion(:,1), dispersion(:,2), self.lambda) + 1i*spline(dispersion(:,1), dispersion(:,3), self.lambda);
                else                                % material refractive index has only n (real part)
                    index   = spline(dispersion(:,1), dispersion(:,2), self.lambda);
                end
                self.n   = [self.n, index];
            end
            self.n       = [self.n, 1];                 % Include air as the last layer.
            assert( length(self.n) == length(self.d) );
        end
        function setKs(self)
            % Calculate total wave-vector in each layer
            self.Ks         = [];
            for iter = 1:length(self.n)
                self.Ks     = [self.Ks, self.n(iter)*self.k_air];
            end
            assert( length(self.Ks) == length(self.n), 'ValueError', 'Dipole must be inside emission layer!' )
        end
        function setQs(self)
            % Calculate k_z vector in each layer. See Eqn.(4) in Inganas paper (1999)
            self.Qs     = [];
            for iter = 1:length(self.n)
                self.Qs = [self.Qs, sqrt(self.Ks(iter)^2-self.Kx^2)];
            end
            assert( length(self.Qs) == length(self.n) );
        end
        function setSourceTerm(self)
            % Calculate source term based on Eqns. (6) and (7) in Benisty paper.
            self.A_TE       = sqrt(3/16/pi);
            self.A_TM_v     = sqrt(3/8/pi)*self.Kx/self.Ks(self.dipole_layer);
            self.A_TM_h     = sqrt(3/16/pi)*self.Qs(self.dipole_layer)/self.Ks(self.dipole_layer);
        end
        function constructInterfaceMatrices(self)
            % Construct interface matrices I based on Inganas paper Eqns. (1), (2), and (3)
            for iter = self.dipole_layer:length(self.n)-1
                self.I_TE_up{iter-self.dipole_layer+1}      = [1, (self.Qs(iter)-self.Qs(iter+1))/(self.Qs(iter)+self.Qs(iter+1)); (self.Qs(iter)-self.Qs(iter+1))/(self.Qs(iter)+self.Qs(iter+1)),1] ...
                                                            /(2*self.Qs(iter)/(self.Qs(iter)+self.Qs(iter+1)));
                self.I_TM_up{iter-self.dipole_layer+1}      = [1,(self.n(iter+1)^2*self.Qs(iter)-self.n(iter)^2*self.Qs(iter+1))/(self.n(iter+1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter+1)); ...
                                                            (self.n(iter+1)^2*self.Qs(iter)-self.n(iter)^2*self.Qs(iter+1))/(self.n(iter+1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter+1)),1]/ ...
                                                            ( (2*self.n(iter)*self.n(iter+1)*self.Qs(iter))/(self.n(iter+1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter+1)) );
            end
            for iter = self.dipole_layer:-1:2
                self.I_TE_dn{self.dipole_layer+1-iter}      = [1, (self.Qs(iter)-self.Qs(iter-1))/(self.Qs(iter)+self.Qs(iter-1)); (self.Qs(iter)-self.Qs(iter-1))/(self.Qs(iter)+self.Qs(iter-1)),1] ...
                                                                /(2*self.Qs(iter)/(self.Qs(iter)+self.Qs(iter-1)));
                self.I_TM_dn{self.dipole_layer+1-iter}      = [1,(self.n(iter-1)^2*self.Qs(iter)-self.n(iter)^2*self.Qs(iter-1))/(self.n(iter-1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter-1)); ...
                                                            (self.n(iter-1)^2*self.Qs(iter)-self.n(iter)^2*self.Qs(iter-1))/(self.n(iter-1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter-1)),1]/ ...
                                                            ( (2*self.n(iter)*self.n(iter-1)*self.Qs(iter))/(self.n(iter-1)^2*self.Qs(iter)+self.n(iter)^2*self.Qs(iter-1)) );
            end
            assert( length(self.I_TE_up) == length(self.I_TM_up) )
        end
        function constructPropMatrix(self)
            % Construct propagation matrices L based on Inganas paper Eqns. (5).
            % Pay special attention to the signs on the exponential, concerning the propagation direction. 
            assert( self.dipole_depth < self.d( self.dipole_layer ) )
            for iter = self.dipole_layer:length(self.n)-1
                if iter==self.dipole_layer
                    self.L_up{iter-self.dipole_layer+1} = [exp(1i*self.Qs(iter)*self.dipole_depth),0;0,exp(-1i*self.Qs(iter)*self.dipole_depth)];
                else
                    self.L_up{iter-self.dipole_layer+1} = [exp(1i*self.Qs(iter)*self.d(iter)),0;0,exp(-1i*self.Qs(iter)*self.d(iter))];
                end
            end
            for iter = self.dipole_layer:-1:2
                if iter==self.dipole_layer
                    self.L_dn{self.dipole_layer+1-iter} = [exp(-1i*self.Qs(iter)*(self.d(iter)-self.dipole_depth)),0;0,exp(1i*self.Qs(iter)*(self.d(iter)-self.dipole_depth))];
                else
                    self.L_dn{self.dipole_layer+1-iter} = [exp(-1i*self.Qs(iter)*self.d(iter)),0;0,exp(1i*self.Qs(iter)*self.d(iter))];
                end
            end
        end
        function constructSourceMatrix(self)                
            % a and b matrices in Eqn. (15) in Benisty paper (1998)
            assert( length(self.L_up) == length(self.I_TE_up) )
            assert( length(self.L_dn) == length(self.I_TE_dn) )
            assert( length(self.I_TE_up)+length(self.I_TE_dn)+1 == length(self.n) )
            self.Aa_TE      = eye(2);
            self.Aa_TM      = eye(2);
            self.Bb_TE      = eye(2);
            self.Bb_TM      = eye(2);
            for iter = 1:length(self.L_up)
                self.Aa_TE  = self.Aa_TE*self.L_up{iter}*self.I_TE_up{iter};
                self.Aa_TM  = self.Aa_TM*self.L_up{iter}*self.I_TM_up{iter};
            end
            for iter = 1:length(self.L_dn)
                self.Bb_TE  = self.Bb_TE*self.L_dn{iter}*self.I_TE_dn{iter};
                self.Bb_TM  = self.Bb_TM*self.L_dn{iter}*self.I_TM_dn{iter};
            end
        end
        function calcE(self)                                
            % Calculate outcoupled electric field, based on Eqn. (16) in Benisty paper (1998)
            % **************** Now only calculate downward emission *****************
            common_denom_TE     = (self.Aa_TE(2,2)*self.Bb_TE(1,1)-self.Aa_TE(1,2)*self.Bb_TE(2,1));
            common_denom_TM     = (self.Aa_TM(2,2)*self.Bb_TM(1,1)-self.Aa_TM(1,2)*self.Bb_TM(2,1));
            self.E0_TE_h        = -(self.Aa_TE(2,2)+self.Aa_TE(1,2))*self.A_TE/common_denom_TE;
            self.E0_TM_v        = (self.Aa_TM(2,2)-self.Aa_TM(1,2))*self.A_TM_v/common_denom_TM;
            self.E0_TM_h        = -(self.Aa_TM(2,2)+self.Aa_TM(1,2))*self.A_TM_h/common_denom_TM;
            self.E2_TE_h        = -(self.Bb_TE(2,1)+self.Bb_TE(1,1))*self.A_TE/common_denom_TE;
            self.E2_TM_v        = (self.Bb_TM(2,1)-self.Bb_TM(1,1))*self.A_TM_v/common_denom_TM;
            self.E2_TM_h        = -(self.Bb_TM(2,1)+self.Bb_TM(1,1))*self.A_TM_h/common_denom_TM;
        end
        function EnergyOC(self)
            % Calculate outcoupled energy based on Poynting vector formulation from electric field
            % Calculated based on Eqn. 13 in Benisty paper
            self.Power_hor_sub_TE   = 2/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E0_TE_h)^2*self.Qs(1)^2/self.Qs(self.dipole_layer)^2);
            self.Power_hor_sub_TM   = 2/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E0_TM_h)^2*self.Qs(1)^2/self.Qs(self.dipole_layer)^2);
            self.Power_hor_sub  = self.Power_hor_sub_TE + self.Power_hor_sub_TM;
            self.Power_ver_sub  = 1/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E0_TM_v)^2*self.Qs(1)^2/self.Qs(self.dipole_layer)^2);
            self.Power_hor_top_TE   = 2/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E2_TE_h)^2*self.Qs(end)^2/self.Qs(self.dipole_layer)^2);
            self.Power_hor_top_TM   = 2/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E2_TM_h)^2*self.Qs(end)^2/self.Qs(self.dipole_layer)^2);
            self.Power_hor_top  = self.Power_hor_top_TE + self.Power_hor_top_TM;            
            self.Power_ver_top  = 1/3*real(self.n(1)/self.n(self.dipole_layer)*abs(self.E2_TM_v)^2*self.Qs(end)^2/self.Qs(self.dipole_layer)^2);
%             self.Power_hor_sub  = 2/3*real(abs(self.E0_TE_h)^2*self.Qs(1)^2/self.Ks(1)^2) +...
%                                   2/3*real(abs(self.E0_TM_h)^2*self.Qs(1)^2/self.Ks(1)^2);
%             self.Power_ver_sub  = 1/3*real(abs(self.E0_TM_v)^2*self.Qs(1)^2/self.Ks(1)^2);
%             self.Power_hor_top  = 2/3*real(abs(self.E2_TE_h)^2*self.Qs(end)^2/self.Ks(end)^2) +...
%                                   2/3*real(abs(self.E2_TM_h)^2*self.Qs(end)^2/self.Ks(end)^2);
%             self.Power_ver_top  = 1/3*real(abs(self.E2_TM_v)^2*self.Qs(end)^2/self.Ks(end)^2);
        end
        function Energy_bottom_air(self)
            % This method applies to BOTTOM emitting OLED only:
            % EnergyOC method calculate how much power goes into the substrate, while this method computes what proportion of that energy escapes from
            % substrate into air.
            % This part of energy is treated incoherently.
            q_air   = sqrt(self.k_air^2-self.Kx^2);
            q_sub   = self.Qs(1);
            n_air   = 1;
            n_sub   = self.n(1);
            self.Power_hor_sub_escape   = self.Power_hor_sub_TE * (1-abs((q_sub-q_air)/(q_sub+q_air))^2) + ...
                                          self.Power_hor_sub_TM * (1-abs((n_air^2*q_sub-n_sub^2*q_air)/(n_air^2*q_sub+n_sub^2*q_air))^2);
            self.Power_ver_sub_escape   = self.Power_ver_sub * (1-abs((n_air^2*q_sub-n_sub^2*q_air)/(n_air^2*q_sub+n_sub^2*q_air))^2);
        end
    end
        
    methods (Static)
        % Path to material refractive indices database
        function index  = getDispersion( matName )
            % disp( matName )
            switch lower(matName)
                case 'alq3'
                    index   = dlmread('./ETLs/Alq3.txt', '', 2, 0);
                case 'alq3_teflon'
                    index   = dlmread('./ETLs/80% of Alq3.txt', '', 2, 0);
                case 'bcp'
                    index   = dlmread('./ETLs/BCP.txt', '', 2, 0);
                case 'npd'
                    index   = dlmread('./HTLs/NPD.txt', '', 2, 0);
                case 'tapc'
                    index   = dlmread('./HTLs/TAPC.txt', '', 2, 0);
                case 'tapc_teflon'
                    index   = dlmread('./HTLs/TAPC and TefAF1600 30pct.txt', '', 2, 0);
                case 'liq'
                    index   = dlmread('./EILs/Liq.txt', '', 2, 0);
                case 'bphen'
                    index   = dlmread('./ETLs/BPhen.txt', '', 2, 0);
                case 'bphen_teflon'
                    index   = dlmread('./ETLs/BPhen and TefAF1600 30pct.txt','',2,0);
                case 'irppy_bphen'
                    index   = dlmread('./EMLs/8pct Irppy-Bphen.txt', '', 2, 0);
                case 'irppy_tapc'
                    index   = dlmread('./EMLs/8pct Irppy-TAPC.txt', '', 2, 0);
                case 'irppy_bphen_teflon'
                    index   = dlmread('./EMLs/BPhen, Irppy 8pct, and TefAF1600 30pct.txt', '', 2, 0);
                case 'irppy_tapc_teflon'
                    index   = dlmread('./EMLs/TAPC, Irppy 8pct, and TefAF1600 30pct.txt', '', 2, 0);
                case 'al'
                    index   = dlmread('./metal/Aluminum.txt', '', 2, 0);
                case 'ag'
                    index   = dlmread('./metal/index_Silver_Johnson.txt', '', 2, 0);
                case 'ito'
                    index   = dlmread('./misc_reference/ITO.txt', '', 2, 0);
                case 'bk7'
                    index   = dlmread('./misc_reference/BK7.txt', '', 2, 0);
                case 'teflon'
                    index   = dlmread('./misc_reference/Teflon AF 1600.txt', '', 2, 0);
                case 'mapbi3'
                    index   = dlmread('./misc_organics/MAPbI3_oxford.txt', '', 2, 0);
                case 'tpbi'
                    index   = dlmread('./ETLs/TPBi.txt', '', 2, 0);
            end
        end
    end
end
