% Outcoupling efficiency for Perovskite LEDs with embedded dipoles
% Vary thickness of HTL and ETL
%===========================================================================
%	 yufei.shen						 02/27/2017			   Version 1.0
%    yufei.shen                      02/09/2018            Version 1.1, Perovskite LED struture for Hoyeon
%===========================================================================

close all;
clear all;
tic;

DEBUG = false;
EL_AVERAGE  = true;     % Boolean, calculation is averaged over emission spectrum if True, otherwise use the peak emission wavelength only. 
% FPATH = 'C:\Users\shenyufei\Dropbox\EE_Research\layeredOrganicEmission_test';           % Please set this to your local path
% cd(FPATH)

wl_peak             = 755;      % Emission peak wavelength
if EL_AVERAGE
    spectrum        = dlmread('./EmissionSpectrum/MAPbI3_emission.txt', '', 2, 0);
    %spectrum        = spectrum( 15:50, : );
else
    spectrum        = [wl_peak, 1];
end
dipoleLayer     = 4;                                                    % Specify which layer dipole is in. Here it is in 'MAPbI3'
materials       = {'BK7', 'ITO','TAPC', 'MAPbI3', 'TPBi', 'Liq', 'Al'};              % Material stack structure, from bottom to top

num_HTL = 50;
num_ETL = 51;
d_HTL	= linspace(5,200,num_HTL);
d_Perov = linspace(5,100,num_ETL);

Total_power     = zeros(length(d_HTL),length(d_Perov));
Total_power_avg = zeros(length(d_HTL),length(d_Perov));         % This is the total OC power averaged over EL spectrum
sumWeights      = sum( spectrum(:,2) );

% Construct the object outside for loop
OLED            = basicStack( wl_peak, dipoleLayer, 1, materials, [nan, ones(1,length(materials)-1)], nan);
OLED.setIndices();
OLED.setKs();
k_para  = linspace(0, OLED.Ks(1), 500);             % Choose how many interpolating points to used between 0 and pi/2 emission angle

for wl_iter = 1:size( spectrum, 1 )
    OLED.lambda     = spectrum( wl_iter );
    if EL_AVERAGE
        fprintf( 'Calculation percentage: %.2f%%\n', wl_iter/size( spectrum, 1 )*100 );
    end
    for HTL_iter = 1:length(d_HTL)
        if ~EL_AVERAGE
            fprintf( 'Calculation percentage: %.2f%%\n', num_ETL*(HTL_iter-1)/(num_HTL*num_ETL)*100 );
        end
        for ETL_iter = 1:length(d_Perov)
            kx_resolved_totalPower  = zeros(1, length(k_para));
            for kx_iter = 1:length(k_para)
                Kx              = k_para(kx_iter);
                OLED.Kx         = Kx;
                % Set thickness of each layer. Same order as in 'materials':
                %                  'BK7','ITO',       'TAPC',          'MAPbl3',    'TPBi','Liq','Al'
                Ds              = [ nan,  150,   d_HTL(HTL_iter),  d_Perov(ETL_iter), 40,  2,   100 ];  
                OLED.d          = [Ds, nan];
                dipoleDepth     = Ds(4)/2;
                OLED.dipole_depth   = dipoleDepth;
                OLED.setQs();
                OLED.setSourceTerm();
                OLED.constructInterfaceMatrices();
                OLED.constructPropMatrix();
                OLED.constructSourceMatrix();
                OLED.calcE();
                OLED.EnergyOC();
                OLED.Energy_bottom_air();
                kx_resolved_totalPower(kx_iter)     = OLED.Power_hor_sub_escape+OLED.Power_ver_sub_escape;
            end
            if DEBUG
                plot( asind(k_para/OLED.k_air), kx_resolved_totalPower );
            end
            outcoupleMask   = k_para < OLED.k_air;
            Total_power(HTL_iter, ETL_iter)         = trapz( asin(k_para(outcoupleMask)/OLED.Ks(1)),...
                                                    2*pi*k_para(outcoupleMask)/OLED.Ks(1).*kx_resolved_totalPower(outcoupleMask) ); % Eqn 19 of Benisty paper
        end
    end
    Total_power_avg = Total_power_avg + Total_power * spectrum( wl_iter, 2 ) / sumWeights;            % Average over MAPbI3 emission
end

[x_grid, y_grid] = meshgrid(d_HTL,d_Perov);
surf( x_grid,y_grid,Total_power_avg' )
xlabel('HTL thickness (nm)','fontsize',30)
ylabel('Perovskite layer thickness (nm)','fontsize',30)
set(gca,'fontsize',30)
az = 0;
el = 90;
view(az, el);
axis tight
grid off
shading interp
colorbar

filename = 'test.mat';
save(filename)


toc;